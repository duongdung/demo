<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Controllers\TestController;

class BasicTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCase1()
    {
        $this->assertEquals("3", Cong(1));
    }

    public function testCase2()
    {
        $this->assertEquals("2", Cong(2));
    }

}
